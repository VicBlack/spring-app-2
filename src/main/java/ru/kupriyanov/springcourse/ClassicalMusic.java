package ru.kupriyanov.springcourse;

public class ClassicalMusic implements IMusic {

    @Override
    public String getMusic() {
        return "classical song";
    }

}
